# Replace with the source docker image
FROM reg.cs.sun.ac.za/computer-science/docker-prebuild/alpine-python3

RUN apk --no-cache add pkgconfig autoconf automake libtool nasm build-base zlib-dev npm
